(defn evaluateFunc [pars els] (mapv #(% pars) els))
(defn operationFunc [f] (fn [& els] (fn [pars] (apply f (evaluateFunc pars els)))))
(defn divFunc [a b] (/ (double a) (double b)))
(defn completeFunc [els] (if (<= (count els) 1) (vec (concat (vector 1.0) els)) els))

(defn constant [c] (fn [pars] c))
(defn variable [var] (fn [pars] (get pars var)))
(defn exp [el] (Math/pow Math/E el))

(def subtract (operationFunc -))
(def add (operationFunc +))
(def multiply (operationFunc *))
(defn divide [& els] (fn [pars] (reduce divFunc (completeFunc (evaluateFunc pars els)))))
(defn negate [el] (fn [pars] (- (el pars))))
(def sumexp (operationFunc (fn [& els] (apply + (mapv exp els)))))
(defn softmax [& els] (fn [pars] (divFunc (exp ((first els) pars)) ((apply sumexp els) pars))))


(def operationsFunc {'- subtract '+ add '* multiply '/ divide 'negate negate 'sumexp sumexp 'softmax softmax})

(defn parseImplFunc [list]
  (if (number? list) (constant list)
                     (if (symbol? list) (variable (name list))
                                        (apply (get operationsFunc (first list)) (mapv parseImplFunc (rest list))))))

(defn parseFunction [expr]
  (parseImplFunc (read-string expr)))




(load-file "proto.clj")
(load-file "parser.clj")


(defn div [a b] (/ (double a) (double b)))
(defn complete [els c] (if (<= (count els) 1) (vec (concat (vector c)  els)) els))
(defn completeNumber [els] (complete els 1))
(defn exp [el] (Math/pow Math/E el))
(defn defoperation [cl symb] (fn [& els] (cl symb els)))

(defclass _Operation _ [symb elements]
          [toString [] (str "(" (_symb this) " " (clojure.string/join " " (mapv _toString (_elements this))) ")")]
          [evaluate [pars] (:abstract)] [diffImpl [dv diffs] (:abstract)]
          [diff [] (eval '(fn [this dv] ((_diffImpl this) this dv (mapv #((_diff %) % dv) (_elements this)))))]
          [toStringInfix [] (let [left (first (_elements this)) right (second (_elements this))]
                              (str "(" (_toStringInfix left) " " (_symb this) " " (_toStringInfix right) ")"))]
          [evalImpl [f pars] (apply f (mapv #(_evaluate % pars) (_elements this)))])

(defclass _Constant _ [c] [evaluate [pars] (_c this)] [toString [] (str (_c this))]
          [diff [] (eval '(fn [this dv] ZERO))] [toStringInfix [] (str (_c this))])

(defclass _Variable _ [v] [evaluate [pars] (get pars (str (Character/toLowerCase (get (_v this) 0))))] [toString [] (str (_v this))]
          [diff [] (eval '(fn [this dv] (if (= dv (_v this)) (_Constant 1) (_Constant 0))))]
          [toStringInfix [] (str (_v this))])

(defclass _Add _Operation [] [evaluate [pars] (_evalImpl this + pars)]
          [diffImpl [] (eval '(fn [this dv diffs] (apply Add diffs)))])

(defclass _Subtract _Operation [] [evaluate [pars] (_evalImpl this - pars)]
          [diffImpl [] (eval '(fn [this dv diffs] (apply Subtract diffs)))])

(defclass _Multiply _Operation [] [evaluate [pars] (_evalImpl this * pars)]
          [diffImpl [] (eval '(fn [this dv diffs] (if (= 1 (count (_elements this)))
                                                    (first diffs) (Add (Multiply (first diffs) (apply Multiply (rest (_elements this))))
                                                                       (Multiply (first (_elements this)) (diff (apply Multiply (rest (_elements this))) dv))))))])

(defclass _Divide _Operation [] [evaluate [pars] (reduce div (completeNumber (mapv #(_evaluate % pars) (_elements this))))]
          [diffImpl [] (eval '(fn [this dv diffs]
                                (let [elements (completeObject (_elements this)) first_elements (if (<= (count elements) 2)
                                                                                                  (first elements) (apply Divide (vec (butlast elements)))) last_element (last elements)]
                                  (if  (= 1 (count elements)) (diff first_elements dv)
                                                              (Divide (Subtract (Multiply (diff first_elements dv) last_element)
                                                                                (Multiply (last diffs) first_elements )) (Multiply last_element last_element))))))])

(defclass _Negate _Operation [] [evaluate [pars] (_evalImpl this - pars)]
          [diffImpl [] (eval '(fn [this dv diffs] (Multiply MINUS_ONE (first diffs))))]
          [toStringInfix [] (str "negate(" (_toStringInfix (first (_elements this))) ")")])

(defclass _Exp _Operation [] [evaluate [pars] (Math/pow Math/E (_evaluate (first (_elements this)) pars))]
          [diffImpl [] (eval '(fn [this dv diffs] (Multiply (Exp (first (_elements this))) (first diffs))))])

(def Exp (defoperation _Exp "e"))

(defclass _Sumexp _Operation [] [evaluate [pars] (apply + (mapv #(exp (_evaluate % pars)) (_elements this)))]
          [diff [] (eval '(fn [this dv] (apply Add (mapv #(diff % dv) (mapv Exp (_elements this))))))])

(def Sumexp (defoperation _Sumexp "sumexp"))

(defclass _Softmax _Operation []
          [evaluate [pars] (div (exp (_evaluate (first (_elements this)) pars)) (_evaluate (apply Sumexp (_elements this)) pars))]
          [diff [] (eval '(fn [this dv] (diff (Divide (Exp (first (_elements this))) (apply Sumexp (_elements this))) dv)))])


(def Constant _Constant)
(def Variable _Variable)
(def Add (defoperation _Add "+"))
(def Subtract (defoperation _Subtract "-"))
(def Multiply (defoperation _Multiply "*"))
(def Divide (defoperation _Divide "/"))
(def Negate (defoperation _Negate "negate"))
(def Softmax (defoperation _Softmax "softmax"))

(def evaluate _evaluate)
(def toString _toString)
(def toStringInfix _toStringInfix)
(defn diff [obj dv] ((_diff obj) obj dv))

(defn completeObject [els] (complete els (Constant 1)))
(def ONE (Constant 1))
(def ZERO (Constant 0))
(def MINUS_ONE (Constant -1))


(defn toLongBits [el] (Double/doubleToLongBits el))

(defclass _BinaryOperation _Operation [f] [evaluate [pars] (let [els (mapv #(_evaluate % pars) (_elements this))]
                                                             (Double/longBitsToDouble (apply (_f this) (mapv toLongBits els))))])

(defn defBinaryOperation [symb f] (fn [& els] (_BinaryOperation symb els f)))
(def BitAnd (defBinaryOperation "&" bit-and))
(def BitOr (defBinaryOperation "|" bit-or))
(def BitXor (defBinaryOperation "^" bit-xor))

(defn bit-impl [a b] (bit-or (bit-not a) b))
(def BitImpl (defBinaryOperation "=>" (comp (partial reduce bit-impl) vector)))
(def BitIff (defBinaryOperation "<=>" (comp bit-not bit-xor)))

(def operations {'- Subtract '+ Add '* Multiply '/ Divide 'negate Negate 'sumexp Sumexp 'softmax Softmax
                 '& BitAnd '| BitOr '^ BitXor '=> BitImpl '<=> BitIff})

(defn parseImpl [list]
  (if (number? list) (Constant list)
                     (if (symbol? list) (Variable (name list))
                                        (apply (get operations (first list)) (mapv parseImpl (rest list))))))

(defn parseObject [expr]
  (parseImpl (read-string expr)))


(defn -show [r] (if (-valid? r) (str "->  " (pr-str (-value r)) " | " (pr-str (-tail r))) "!"))
(defn -tabulate [p & inputs] (run! (fn [input] (printf "  %-10s %s\n" (pr-str input) (-show (p input)))) inputs))

(defn _sign [s tail] (if (#{\- \+} s) (cons s tail) tail))
(defn _concat [left mid right] (if (-valid? mid) (concat left (cons mid right)) left))
(defn _apply-unary-operator [[op a]] (if (-valid? op) (op a) a))
(defn _apply-binary-operator [left right] ((first right) left (second right)))
(defn _fold-right [f coll val] (if (empty? coll) val
                                                 (f (first coll) (_fold-right f (rest coll) val))))

(defn _apply-binary-operator-right [left right] ((second left) (first left) right))

(def *digit (+char "1234567890"))
(def +number (+map read-string (+str (+seqf _sign (+opt (+char "-+"))
                                            (+str (+seqf _concat (+plus *digit) (+opt (+char ".")) (+star *digit)))))))
(def *number (+map Constant +number))

(def *all-chars (mapv char (range 32 128)))
(def *all-letters (apply str (filter #(Character/isLetter %) *all-chars)))
(def *letter (+char *all-letters))
(def *variable (+map Variable (+str (+seqf cons *letter (+star *letter)))))

(def *space (+char " \t\n\r"))
(def *ws (+ignore (+star *space)))

(defn +string [string] (+map (constantly string) (apply +seq (mapv (comp +char str) (seq (name string))))))
(defn +operation [operations] (apply +or (mapv +string operations)))
(defn *operation [ops] (+map (partial (partial get ops)) (+operation (keys ops))))

(declare *priority8)
(def *priority0 (+opt (+seqn 0 *ws (+or *number *variable (+seqn 1 (+char "(") (delay *priority8) (+char ")"))) *ws)))

(defn +priority-left [prev operation] (+map (partial apply (partial reduce _apply-binary-operator))
                                            (+opt (+seq *ws prev *ws (+star (+seq operation *ws prev *ws))))))


(defn +priority-right [prev operation] (+map (partial apply (partial _fold-right _apply-binary-operator-right))
                                             (+opt (+seq *ws (+star (+seq prev *ws operation *ws)) prev *ws ))))

(def *operation1 (*operation {'negate Negate}))
(def *priority1 (+opt (+or (+map (comp _apply-unary-operator (juxt #(nth % 0) #(nth % 1)))
                                 (+seq *ws *operation1 *ws (+or (delay *priority1) *priority0) *ws) ) *priority0)))

(defn *priority [ops prev lr] (lr prev (*operation ops)))

(def *priority2 (*priority {'* Multiply '/ Divide} *priority1 +priority-left))
(def *priority3 (*priority {'+ Add '- Subtract} *priority2 +priority-left))

(def *priority4 (*priority {'& BitAnd} *priority3 +priority-left))
(def *priority5 (*priority {'| BitOr} *priority4 +priority-left))
(def *priority6 (*priority {(symbol "^") BitXor} *priority5 +priority-left))

(def *priority7 (*priority {'=> BitImpl} *priority6 +priority-right))
(def *priority8 (*priority {'<=> BitIff} *priority7 +priority-left))

(def parseObjectInfix (+parser (+seqn 0 *ws *priority8 *ws)))

